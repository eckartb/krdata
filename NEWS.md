# krdata 0.2.0

Added dataset `k_wine`: it is data of whitewines with chemical attributes
as well as a quality score ranging between 0 and 10. This dataset is useful
for regression analysis.

# krdata 0.1.2

Added `housevotes84b` data set. It is based on the data set `housevotes84` (which in turn is based on the data set `HouseVotes84` from the R package `mlbench`) with the difference that all observations are removed that contain at least on missing value (`NA`).

# krdata 0.1.1

Added data under name `k_churn`. It is a reformatted version of the data set `churn` from the R package `C50`. It contains data of telecom customers (their phone usage combined with corresponding rates) and the outcomes of "churn" (attrition).

