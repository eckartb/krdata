#' UCI Whitewine Data
#'
#' This dataset if a processed version of the data
#' available at the UCI machine learning repository
#' <https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality>
#' It has been row-shuffled and split into testing and training sets.
#' Also, the training data and labels are separate, leading to 4
#' variables: `k_wine_train_data`,`k_wine_train_label`,`k_wine_test_data`
#' `k_wine_test_label`. The "label" to be predicted are scores
#' between 0 and 10, making this a usable dataset for regression.
#'
#' @name k_wine
#' @param fixed.acidity A measure of "fixed acidity"
#' @param volatile.acidity A measure of "volative acidity"
#' @param citric.acid A measure of citric acid content
#' @param residual.sugar A measure of residual sugar
#' @param chlorides A measure of chloride ion content
#' @param free.sulfur.dioxide A measure of free sulfur dioxide
#' @param total.sulfur.dioxide A measure of total sulfur dioxide
#' @param density The density of the wine
#' @param pH The pH value
#' @param sulphates A measure of sulphate content
#' @param alcohol A measure of alcogol content
#' @param quality A quality score (integer) ranging from 0 to 10
#' @source <https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality>
#' @references {
#'   P. Cortez, A. Cerdeira, F. Almeida, T. Matos and J. Reis.
#'   Modeling wine preferences by data mining from physicochemical properties.
#'   In Decision Support Systems, Elsevier, 47(4):547-553. ISSN: 0167-9236.
#' }
NULL

#' Spiders Following Logistic Distribution
#'
#' Suzuki et al reported a dependence of the presence of wolf spiders
#' and the size of sand corn grains. The spiders appear to be more
#' prevalen on beaches with large sand grain sizes. The dataset is
#' included here to practice logistic regression.
#'
#' @name k_spiders
#' @param Grainsize Size of sand grains in millimeters corresponding to 29 beaches in Japan
#' @param Spiders Logic variable indicating the presence or absence of the Wolf Spider (Lycosa ishikariana)
#' on the corresponding beach.
#' @references {
#' Suzuki, S., N. Tsurusaki, and Y. Kodama. 2006.
#' Distribution of an endangered burrowing spider Lycosa ishikariana in the San'in Coast of Honshu, Japan (Araneae: Lycosidae)
#' Acta Arachnologica 55: 79-86
#' }
NULL

#' Study Hours Versus Exam Success
#'
#' This fictional dataset contains for a set of 20 students the number of
#' hours they studied for a certain test and a 1 or 0 depending on
#' whether they passed the exam or not. This dataset is useful for
#' demonstrating the use of logistic regression. It was obtained from
#' <https://en.wikipedia.org/wiki/Logistic_regression>, accessed Feb 18, 2019.
#'
#' @name k_studyhours
#' @param Hours Number of hours studied for a certain test
#' @param Pass 1 or 0 depending on whether the exam was passed by the student or not.
#' @source <https://en.wikipedia.org/wiki/Logistic_regression>
NULL
