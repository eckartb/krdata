# Data Set of Whitewine Quality

This prepares the "whitewine" dataset from the UCI machine learning repository for our machine learning class.

Downloaded from <https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/> on February 18, 2019

## How to reproduce?

Running the script: `Rscript prepare_winedata.R` will create an R object file called `k_wine.rda`. Placing this file into the data directory results in the final product.
