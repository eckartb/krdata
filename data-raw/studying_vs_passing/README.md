# Study Hourse Versus Passing of Test

This fictional dataset is taken from <https://en.wikipedia.org/wiki/Logistic_regression> in order to demonstrate logistic regression. Accessed at February 18, 2019

## How to reproduce

`Rscript prepare_studygours.R` will create a data name `studyhours.rda` in the data subdirectory unless this already exists (in this case you might have to add the option overwrite=TRUE to the `usethis::use_data` command).
